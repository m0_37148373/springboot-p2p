package com.ddmh.p2p.socket.model;

import lombok.Getter;

import java.io.Serializable;

/**
 * 用于表示文件的数据结构
 *
 * @author brant
 * @since 2024-04-12
 */
@Getter
public class SharedFile implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String name;
    private final long size;

    public SharedFile(String name, long size) {
        this.name = name;
        this.size = size;
    }
}