package com.ddmh.p2p.socket.client;

import lombok.Getter;

import java.io.Serializable;

/**
 * 下载请求类
 *
 * @author brant
 * @since 2024-04-12
 */
@Getter
public class DownloadRequest implements Serializable {
    private final String fileName;
    private final String requestingClientAddress;
    private final int requestingClientPort;

    public DownloadRequest(String fileName, String requestingClientAddress, int requestingClientPort) {
        this.fileName = fileName;
        this.requestingClientAddress = requestingClientAddress;
        this.requestingClientPort = requestingClientPort;
    }
}