package com.ddmh.p2p.socket;

import com.ddmh.p2p.socket.client.Client;
import com.ddmh.p2p.socket.server.Server;
import org.junit.Test;

public class P2PTest {
    @Test
    public void startServer() {
        // 启动P2P服务器
        Server server = new Server(12345);
        server.start();
    }

    @Test
    public void startClient() {
        // 启动P2P客户端并共享文件
        Client client = new Client("localhost", 12345);
        client.addSharedFile("C:\\Users\\Brant\\Downloads\\memory.csv");
        client.start();
    }

    @Test
    public void startClient2() {
        // 启动P2P客户端并共享文件
        Client client = new Client("localhost", 12345);
        client.addSharedFile("C:\\Users\\Brant\\Downloads\\20240331-0056-47.98.216.245.txt");
        client.start();
    }
}
